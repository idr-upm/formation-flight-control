format long
mu = 3.04042345394847e-06;
rs = 149181538551.12744;
ts = 5.019110285346012e+06;
x_L2 = L2_position(mu)*rs;

A11 = zeros(3);
A12 = eye(3);
alfa = (1-mu)/(x_L2/rs+mu)^3 + mu/(x_L2/rs-1+mu)^3;
A21 = [2*alfa 0 0; 0 -alfa 0; 0 0 -alfa] + [1 0 0; 0 1 0; 0 0 0];
A22 = [2 0 0; 0 -2 0; 0 0 0];
A = [A11 A12; A21 A22]; %matrix A

%%
chief_orbit = readtable('chief_orbit.csv'); %read table
state = chief_orbit(:, 2:7);    %select desirable columns (x y z vx vy vz)
state = state{:,:}; %convert table to matrix

for i = 1:size(state,1)
    
    x = state(i,1);
    y = state(i,2);
    z = state(i,3);
    xp = state(i,4);
    yp = state(i,5);
    zp = state(i,6);
    
    %Non-Linear model
    rho1 = sqrt((x+mu)^2 + y^2 + z^2);
    rho2 = sqrt((x-(1-mu))^2 + y^2 + z^2);
    xpp = x + 2*yp - (1-mu)*(x+mu)/ro1^3 - mu*(x-(1-mu))/ro2^3;
    ypp = y - 2*xp - (1-mu)*y/ro1^3 - mu*y/ro2^3;
    zpp = -(1-mu)*z/ro1^3 - mu*z/ro2^3;
    NL_state_derivative = [xp yp zp xpp ypp zpp];
    
    %Linear model
    L_state_derivative = A*state(i,:)';
    
    %Discrepancy vector
    discrep_state = abs(NL_state_derivative - L_state_derivative);
    discrep_x(i) = discrep_state(1);
    discrep_y(i) = discrep_state(2);
    discrep_z(i) = discrep_state(3);
    discrep_vx(i) = discrep_state(4);
    discrep_vy(i) = discrep_state(5);
    discrep_vz(i) = discrep_state(6);
    
end

W = zeros(6);
W(1,1) = (mean(discrep_x))^2/rs^2;
W(2,2) = (mean(discrep_y))^2/rs^2;
W(3,3) = (mean(discrep_z))^2/rs^2;
W(4,4) = (mean(discrep_vx))^2/(rs/ts)^2;
W(5,5) = (mean(discrep_vy))^2/(rs/ts)^2;
W(6,6) = (mean(discrep_vz))^2/(rs/ts)^2;
