function LP = lagrange_points(mu)
format long
LP = zeros(5,3); %matriz que almacena por columnas las coordenadas de los puntos de lagrange

%L1
p_L1 = [1, 2*(mu-1), 1^2-4*1*mu+mu^2, 2*mu*1*(1-mu)+(mu-1), mu^2*1^2+2*(1^2+mu^2), (mu^3-1^3)];
L1roots = roots(p_L1);
L1=0;
for i=1:5
    if (L1roots(i) > -mu) && (L1roots(i) < 1-mu)
        L1 = L1roots(i);
    end
end
LP(1,1) = L1;

%L2
p_L2 = [1, 2*(mu-1), 1^2-4*1*mu+mu^2, 2*mu*1*(1-mu)-(mu+1), mu^2*1^2+2*(1^2-mu^2), -(mu^3+1^3)];
L2roots = roots(p_L2);
L2=0;
for i=1:5
    if (L2roots(i) > -mu) && (L2roots(i) > 1-mu)
        L2 = L2roots(i);
    end
end
LP(2,1) = L2;

%L3
p_L3 = [1, 2*(mu-1), 1^2-4*1*mu+mu^2, 2*mu*1*(1-mu)+(mu+1), mu^2*1^2+2*(mu^2-1^2), (mu^3+1^3)];
L3roots = roots(p_L3);
L3=0;
for i=1:5
    if (L3roots(i) < -mu) && (L3roots(i) < 1-mu)
        L3 = L3roots(i);
    end
end
LP(3,1) = L3;

%equilateral
LP(4,1) = 1/2-mu; LP(4,2) = sqrt(3)/2;
LP(5,1) = 1/2-mu; LP(5,2) = -sqrt(3)/2;
end
