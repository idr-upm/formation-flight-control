function U = crtbp_potential(mu, x, y, z)
x_m1 = -mu;
x_m2 = 1-mu;

r1 = sqrt((x-x_m1).^2+y.^2+z.^2);
r2 = sqrt((x-x_m2).^2+y.^2+z.^2);

U = 1/2*(x.^2+y.^2)+(1-mu)./r1+mu./r2;
end