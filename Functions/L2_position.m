function x = L2_position(mu)
format long

p_L2 = [1, 2*(mu-1), 1^2-4*1*mu+mu^2, 2*mu*1*(1-mu)-(mu+1), mu^2*1^2+2*(1^2-mu^2), -(mu^3+1^3)];
L2roots = roots(p_L2);

L2=0;
for i=1:5
    if (L2roots(i) > -mu) & (L2roots(i) > 1)
        L2 = L2roots(i);
    end
end
x = L2;
end