%% Estimador �ptimo para no control (4h)

% X
figure(1)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,1)*1e3, 'k')
ylabel({'Error en la';'posici�n en x';'[mm]'})
% Y
figure(2)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,2)*1e3, 'k')
ylabel({'Error en la';'posici�n en y';'[mm]'})
% Z
figure(3)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,3)*1e3, 'k')
ylabel({'Error en la';'posici�n en z';'[mm]'})
% VX
figure(4)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,4)*1e6, 'k')
ylabel({'Error en la';'velocidad en x';'[\mum/s]'})
% VY
figure(5)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,5)*1e6, 'k')
ylabel({'Error en la';'velocidad en y';'[\mum/s]'})
% VZ
figure(6)
plot(out.estimador_optimo.Time*(ts/60), out.estimador_optimo.Data(:,6)*1e6, 'k')
xlabel('Tiempo [min]')
ylabel({'Error en la';'velocidad en z';'[\mum/s]'})


rms_x = rms(out.estimador_optimo.Data(:,1)*1e3)
rms_y = rms(out.estimador_optimo.Data(:,2)*1e3)
rms_z = rms(out.estimador_optimo.Data(:,3)*1e3)
rms_vx = rms(out.estimador_optimo.Data(:,4)*1e6)
rms_vy = rms(out.estimador_optimo.Data(:,5)*1e6)
rms_vz = rms(out.estimador_optimo.Data(:,6)*1e6)