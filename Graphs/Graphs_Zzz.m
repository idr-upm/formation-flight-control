%% Para graficar la propagaci�n sin control
% 1 semana
% se grafica el estado real

figure(1)
plot(out.sin_control_ref.Time*(ts/(3600*24)), out.sin_control_ref.Data, 'k')
xlabel('Tiempo [d�as]')
ylabel({'Distancia [m]'})
