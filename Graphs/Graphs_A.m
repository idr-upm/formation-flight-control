%% Estimador simple para no control (4h, w01=1.3e04)

% X
figure(1)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,1)*1e3, 'k')
ylabel({'Error en la';'posici�n en x';'[mm]'})
% Y
figure(2)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,2)*1e3, 'k')
ylabel({'Error en la';'posici�n en y';'[mm]'})
% Z
figure(3)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,3)*1e3, 'k')
ylabel({'Error en la';'posici�n en z';'[mm]'})
% VX
figure(4)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,4)*1e6, 'k')
ylabel({'Error en la';'velocidad en x';'[\mum/s]'})
% VY
figure(5)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,5)*1e6, 'k')
ylabel({'Error en la';'velocidad en y';'[\mum/s]'})
% VZ
figure(6)
plot(out.estimador_simple.Time*(ts/60), out.estimador_simple.Data(:,6)*1e6, 'k')
xlabel('Tiempo [min]')
ylabel({'Error en la';'velocidad en z';'[\mum/s]'})


rms_x = rms(out.estimador_simple.Data(:,1)*1e3)
rms_y = rms(out.estimador_simple.Data(:,2)*1e3)
rms_z = rms(out.estimador_simple.Data(:,3)*1e3)
rms_vx = rms(out.estimador_simple.Data(:,4)*1e6)
rms_vy = rms(out.estimador_simple.Data(:,5)*1e6)
rms_vz = rms(out.estimador_simple.Data(:,6)*1e6)