%% Modelo �ptimo
% 1. Posici�n relativa de simulaci�n completa
figure(1)
plot(out.control_optimo.Time*(ts/60), out.control_optimo.Data, 'k')
xlabel('Tiempo [min]')
ylabel({'Error en la posici�n [m]'})

% 2. Posici�n relativas en etapa 2
%Detectar cuando es el cambio de etapa y a�adir unas cuantas posiciones
%para que no grafique el transitorio
cambio = round(out.cambio_opt.Data(end,1)*size(out.control_optimo.Time,1)/300)+2500;
% X
figure(2)
plot(out.control_optimo.Time(cambio:end)*(ts/60), out.control_optimo.Data((cambio:end),1)*1e6, 'k')
ylabel({'Error en la';'posici�n en x';'[\mum]'})
% X zoom
zoom = round(180*size(out.control_optimo.Time,1)/300);
figure(11)
plot(out.control_optimo.Time(zoom:end)*(ts/60), out.control_optimo.Data((zoom:end),1)*1e6, 'k')
xlabel('Tiempo [min]')
ylabel({'Error en la';'posici�n en x';'[\mum]'})
% Y
figure(3)
plot(out.control_optimo.Time(cambio:end)*(ts/60), out.control_optimo.Data((cambio:end),2)*1e6, 'k')
ylabel({'Error en la';'posici�n en y';'[\mum]'})
% Z
figure(4)
plot(out.control_optimo.Time(cambio:end)*(ts/60), out.control_optimo.Data((cambio:end),3)*1e6, 'k')
xlabel('Tiempo [min]')
ylabel({'Error en la';'posici�n en z';'[\mum]'})

% 3. Cambio entre etapas (se saca de C.1 haciendo zoom)

% 4. Impulso
% X
figure(5)
plot(out.impulso.Time(cambio:end)*(ts/60), out.impulso.Data((cambio:end),1)*1e6, 'k')
ylabel({'Impulso en x';'[\muN]'})
% Y
figure(6)
plot(out.impulso.Time(cambio:end)*(ts/60), out.impulso.Data((cambio:end),2)*1e6, 'k')
ylabel({'Impulso en y';'[\muN]'})
% Z
figure(7)
plot(out.impulso.Time(cambio:end)*(ts/60), out.impulso.Data((cambio:end),3)*1e6, 'k')
xlabel('Tiempo [min]')
ylabel({'Impulso en z';'[\muN]'})

% 5. Impulso sobre comando
% Cotas para graficar el momento del cambio de etapa
izq = 3000;
der = -1000;
% X
figure(8)
plot(out.impulso.Time(cambio-izq:cambio+der)*(ts/60), [out.impulso.Data((cambio-izq:cambio+der),1)*1e6, out.comando.Data((cambio-izq:cambio+der),1)*1e6], 'k')
ylabel({'Impulso en x';'[\muN]'})
% Y
figure(9)
plot(out.impulso.Time(cambio-izq:cambio+der)*(ts/60), [out.impulso.Data((cambio-izq:cambio+der),2)*1e6, out.comando.Data((cambio-izq:cambio+der),2)*1e6], 'k')
ylabel({'Impulso en y';'[\muN]'})
% Z
figure(10)
plot(out.impulso.Time(cambio-izq:cambio+der)*(ts/60), [out.impulso.Data((cambio-izq:cambio+der),3)*1e6, out.comando.Data((cambio-izq:cambio+der),3)*1e6], 'k')
xlabel('Tiempo [min]')
ylabel({'Impulso en z';'[\muN]'})




% Precisi�n en posicionamiento relativo durante la etapa 2
error_x = rms(out.control_optimo.Data((zoom:end),1)*1e6)
error_y = rms(out.control_optimo.Data((cambio:end),2)*1e6)
error_z = rms(out.control_optimo.Data((cambio:end),3)*1e6)




%% Impulso total ejercido (en Newtons)
imp_et1_x = sum(abs(out.impulso.Data((1:cambio),1)))
imp_et1_y = sum(abs(out.impulso.Data((1:cambio),2)))
imp_et1_z = sum(abs(out.impulso.Data((1:cambio),3)))
imp_et2_x = sum(abs(out.impulso.Data((cambio:end),1)))
imp_et2_y = sum(abs(out.impulso.Data((cambio:end),2)))
imp_et2_z = sum(abs(out.impulso.Data((cambio:end),3)))



