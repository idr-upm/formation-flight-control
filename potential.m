mu_ = 7.346120141066527e22/(5.97243694186395e24+7.346120141066527e22); %earth-moon
%% Masses m1 and m2
x_m1 = -mu_;
x_m2 = 1-mu_;

plot(x_m1,0,'ko','MarkerSize',7,'MarkerFaceColor','b')
hold on
plot(x_m2,0,'ko','MarkerSize',5,'MarkerFaceColor','k')
hold on


%% Lagrange points
LP = lagrange_points(mu_);

plot(LP(:,1), LP(:,2), 'r+')
hold on


%% Zero velocity curves
[X,Y] = meshgrid(-2:0.01:2);
U = crtbp_potential(mu_, X, Y, 0);
LP1_level = crtbp_potential(mu_, LP(1,1), LP(1,2), 0);
LP2_level = crtbp_potential(mu_, LP(2,1), LP(2,2), 0);
LP3_level = crtbp_potential(mu_, LP(3,1), LP(3,2), 0);
v = [1.55, 1.7];

contour(X,Y,U,[LP1_level LP2_level LP3_level v]); %'ShowText','on'
colorbar
hold on

xlabel('x')
ylabel('y')
axis square
axis equal
xlim([-1.8 1.8])
ylim([-1.8 1.8])
