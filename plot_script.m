%%%Plotting results and data


%%Plot of simulink orbit
% tra = out.chief_trajectory(:,1:3);
% x = tra(:,1);
% y = tra(:,2);
% z = tra(:,3);
% plot3(x, y, z, 'g*')
% hold on


%%Plot of original orbit
plot3(mubody_orbit.x, mubody_orbit.y, mubody_orbit.z)
hold on
%L2
plot3(L2_position(mu), 0, 0, 'r*')
hold on
%earth-moon
plot3(1-mu, 0, 0, 'ko','MarkerSize',7,'MarkerFaceColor','b')
hold on

grid