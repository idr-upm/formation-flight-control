%Using long format so small numbers are not rounded
format long

%CRTBP constants from GMAT
G = 6.673e-11; %N*m^2*kg^(-2)
mass_earth_moon = 5.97243694186395e24 + 7.346120141066527e22; %kg
mass_sun = 1.988499251093647e30; %kg

%Mass parameter
mu = mass_earth_moon/(mass_sun + mass_earth_moon); 

%Length used for adimensionalization 
rs = 149181538551.12744; %m

%Earth orbit period
T = 365*86400; %s

%Adimensionalization factor for time
ts = T/(2*pi); %s
%ts = 1/(sqrt(G*(mass_earth_moon + mass_sun)/(rs^3)));
%el ts usado en la adimensionalizacion convencial siendo omega la velocidad angular del SR sin�dico respecto al baricentro (muy parecido al ts usado)

%L2 dimensional position
x_L2 = L2_position(mu)*rs;

%Earth (earth-moon) dimensional position
x_earth = (1-mu)*rs;

%Satellites properties from CDF study
deputy_mass = 7; %kg

%Initial chief state from mubody, DC orbit
chief_orbit = readtable('chief_orbit.csv');
x0 = chief_orbit.x(1);
y0 = chief_orbit.y(1);
z0 = chief_orbit.z(1);
vx0 = chief_orbit.vx(1);
vy0 = chief_orbit.vy(1);
vz0 = chief_orbit.vz(1);

chief_state0 = [x0 y0 z0 vx0 vy0 vz0]';

deputy_state0 = chief_state0 + [300 0 0 0 0 0]'/rs;

x0_estimator = [310 -5 1 0 0 0]'/rs; %initial conditions for the continuous control observer's integrator

%Reference
y_ref = [300 0 0]'/rs; %desired relative position

%Switchs
estimator_switch = 3; %1 for simple, 2 for optimum, 3 for real state (to test the control)
controller_switch = 3; %1 for simple, 2 for optimum, 3 for ETM, 4 for no control

%Simulation time
Tsim = 2*pi/(365)*3;


%% Calculus of matrices K (controllability) and L (observability)

%Linearized system
A11 = zeros(3);
A12 = eye(3);
alfa = (1-mu)/(x_L2/rs+mu)^3 + mu/(x_L2/rs-1+mu)^3;
A21 = [2*alfa 0 0; 0 -alfa 0; 0 0 -alfa] + [1 0 0; 0 1 0; 0 0 0];
A22 = [2 0 0; 0 -2 0; 0 0 0];
A = [A11 A12; A21 A22]; %matrix A

B = [zeros(3); eye(3)]; %matrix B

C = [eye(3) zeros(3)]; %matrix C

%Controllability
Q = ctrb(A,B); %controllability matrix
rank(Q);
accuracy = 0.1; %m
crono = 10*60/ts;

%Coarse
w01 = 1.3e4;
p1 = zeros(1,6); %desired poles
for j=0:2:4
    arg = (pi/2 + pi/12) + j*pi/6;
    z = w01*(cos(arg) + 1i*sin(arg));
    p1(j+1) = z;
    p1(j+2) = conj(z);
end
K1 = place(A,B,p1); %controller gain matrix   
k1 = -(C*(A-B*K1)^(-1)*B)^(-1);

%Tight
w02 = 7.1e5;
p2 = zeros(1,6); %desired poles
for j=0:2:4
    arg = (pi/2 + pi/12) + j*pi/6;
    z = w02*(cos(arg) + 1i*sin(arg));
    p2(j+1) = z;
    p2(j+2) = conj(z);
end
K2 = place(A,B,p2); %controller gain matrix   
k2 = -(C*(A-B*K2)^(-1)*B)^(-1);

%Observability
N = obsv(A,C); %observability matrix
rank(N);

q1 = p1; %same eigenvlues as in coarse controllability

L1 = place(A',C',q1)'; %observer gain matrix


%% Optimum controller

%Coarse
x1_1 = 5e-4/rs;
x1_2 = 5e-4/rs;
x1_3 = 5e-4/rs;
x1_4 = 5e-4/(rs/ts);
x1_5 = 5e-4/(rs/ts);
x1_6 = 5e-4/(rs/ts);
u1_1 = 5e-10/(rs/ts^2);
u1_2 = 5e-10/(rs/ts^2);
u1_3 = 5e-10/(rs/ts^2);
rho1 = 1;

Q1 = [1/x1_1^2, 1/(2*x1_1*x1_2), 0, 1/(2*x1_1*x1_4), 1/(2*x1_1*x1_5), 0;...
    1/(2*x1_2*x1_1), 1/x1_2^2, 0, 1/(2*x1_2*x1_4), 1/(2*x1_2*x1_5), 0;...
    0, 0, 1/x1_3^2, 0, 0, 1/(2*x1_3*x1_6);...
    1/(2*x1_4*x1_1), 1/(2*x1_4*x1_2), 0, 1/x1_4^2, 1/(2*x1_4*x1_5), 0;...
    1/(2*x1_5*x1_1), 1/(2*x1_5*x1_2), 0, 1/(2*x1_5*x1_4), 1/x1_5^2, 0;...
    0, 0, 1/(2*x1_6*x1_3), 0, 0, 1/x1_6^2];
R1 = rho1*[1/u1_1^2, 1/(2*u1_1*u1_2), 0;...
    1/(2*u1_2*u1_1), 1/u1_2^2, 0;...
    0, 0, 1/u1_3^2];

[K1_opt,S1,e1] = lqr(A,B,Q1,R1,zeros(6,3));
k1_opt = -(C*(A-B*K1_opt)^(-1)*B)^(-1);

%Tight
x2_1 = 1e-6/rs;
x2_2 = 1e-6/rs;
x2_3 = 1e-6/rs;
x2_4 = 1e-6/(rs/ts);
x2_5 = 1e-6/(rs/ts);
x2_6 = 1e-6/(rs/ts);
u2_1 = 1e-8/(rs/ts^2);
u2_2 = 1e-8/(rs/ts^2);
u2_3 = 1e-8/(rs/ts^2);
rho2 = 1;

Q2 = [1/x2_1^2, 1/(2*x2_1*x2_2), 0, 1/(2*x2_1*x2_4), 1/(2*x2_1*x2_5), 0;...
    1/(2*x2_2*x2_1), 1/x2_2^2, 0, 1/(2*x2_2*x2_4), 1/(2*x2_2*x2_5), 0;...
    0, 0, 1/x2_3^2, 0, 0, 1/(2*x2_3*x2_6);...
    1/(2*x2_4*x2_1), 1/(2*x2_4*x2_2), 0, 1/x2_4^2, 1/(2*x2_4*x2_5), 0;...
    1/(2*x2_5*x2_1), 1/(2*x2_5*x2_2), 0, 1/(2*x2_5*x2_4), 1/x2_5^2, 0;...
    0, 0, 1/(2*x2_6*x2_3), 0, 0, 1/x2_6^2];
R2 = rho2*[1/u2_1^2, 1/(2*u2_1*u2_2), 0;...
    1/(2*u2_2*u2_1), 1/u2_2^2, 0;...
    0, 0, 1/u2_3^2];

[K2_opt,S2,e2] = lqr(A,B,Q2,R2,zeros(6,3));
k2_opt = -(C*(A-B*K2_opt)^(-1)*B)^(-1);


%% Noise

%Measure

delta_r = (1e-4); %[m]
delta_theta = pi/(180*14400); %[rad]
delta_phi = pi/(180*14400); %[rad]

V_sensors = zeros(3); %measurement covariance matrix in spherical coordiantes
V_sensors(1,1) = (delta_r)^2/(rs)^2;
V_sensors(2,2) = (delta_theta)^2;
V_sensors(3,3) = (delta_phi)^2;

V_Kalman = zeros(3); %measurement covariance matrix in cartesian coordinates
V_Kalman(1,1) = (1e-4)^2/(rs)^2;
V_Kalman(2,2) = (1e-4)^2/(rs)^2;
V_Kalman(3,3) = (1e-4)^2/(rs)^2;

% Model

W = zeros(6);
W(1,1) = 0/rs^2;
W(2,2) = 0/rs^2;
W(3,3) = 0/rs^2;
W(4,4) = (1e-3)^2/(rs/ts)^2;
W(5,5) = (1e-3)^2/(rs/ts)^2;
W(6,6) = (1e-3)^2/(rs/ts)^2;





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Discrete control
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

delta_t = 6*3600/ts;
D = [eye(3); zeros(3)];




